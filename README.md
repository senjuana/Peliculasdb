![Image](http://i.imgur.com/8AWyo3f.png)


# Projecto de conexion con DB
![Image](http://i.imgur.com/VtjMJ9x.png)

## About
Este es un repositorio publico que existe para poder guardar un projecto hecho en java para tener un CRUD 
completo enfocado en peliculas.

## Requirements
Supported operating systems:
* Development enviroments: GNU/Linux, MacOS X.

Requirements:
* JDK 8
* mysql/mariadb

## Getting the code
	$ git clone https://senjuana@gitlab.com/senjuana/Peliculasdb.git
	$ cd Peliculasdb


# Contributing
Si encuentras un bug en el codigo o un issue, por favor notificame de manera privada en
[mi cuenta personal de twitter](https://twitter.com/senjuana).

Este proyecto sigue [code of merit](https://github.com/rosarior/Code-of-Merit). En este repositorio, me importa el codigo,
no opiniones personales o sentimientos. Espero tratar con  adultos.

Antes de enviar un pedazo de codigo por favor verifica que lo que envias funciona, yo no soy la persona que resolvera tus problemas con tu codigo.

# FAQ

* **Cual es el punto de compartir tu codigo?**

    Mi unico interes es el de poder compartir mi projecto como  herramienta de aprendizaje.

* **Puedo utilizar este codigo en mis proyectos?**

    Siempre y cuando sigas los parametros de la licencia cualquiera puede utilizar o modiificar las implementaciones de este repositorio para cualquier proyecto personal.

## Realeses 
[versiones](https://gitlab.com/senjuana/Peliculasdb/tags)


